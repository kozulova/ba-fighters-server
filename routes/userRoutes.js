const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const userService = require('../services/userService');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next)=>{
    try{
        res.data = userService.showAll();
    }
    catch(err){
        res.data = err;
    }
    finally{
        next()
    }  
}, responseMiddleware);

router.post('/', createUserValid,  (req, res, next)=>{
if(res.err){
    next();
}else{
    try{
        const user = req.body;
        const data = userService.create(user);
        res.data = data;
    }
    catch(err){
        res.err = err;
    }
    finally{
        next();
    }
}
}, responseMiddleware)

router.get('/:id', (req, res, next)=>{ 
    try{
        const id = req.params;
        const data = userService.search(id);
        res.data = data;
    }
    catch(err){
        res.err = err;
    }
    finally{
        next();
    }
}, responseMiddleware)


router.put('/:id', updateUserValid, (req, res, next)=>{ 
    if(res.err){
        next();
    }else{
        try{
            const id = req.params.id;
            const user = req.body;
            const data = userService.update(id, user);
            res.data = data;
        }
        catch(err){
            res.err = err;
        }
        finally{
            next();
        }
    }
}, responseMiddleware)

router.delete('/:id', (req, res, next)=>{ 
    try{
        const id = req.params.id;
        const data = userService.delete(id);
        res.data = data;
    }
    catch(err){
        res.err = err;
    }
    finally{
        next();
    }
}, responseMiddleware)

module.exports = router;