const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const newFighter = req.body;

    Object.keys(fighter).forEach(key => {
        if (key != 'id' && key != 'health') {
            if (Object.keys(newFighter).indexOf(key) < 0) {
                res.err = Error(`${key} is missing`);
                next();
            }
        }
    });

    Object.keys(newFighter).forEach(key => {
        if (key === "id") {
            res.err = Error('id property is not allowed');
            next();
        } else if (Object.keys(fighter).indexOf(key) < 0) {
            res.err = Error(`${key} is not allowed attribute`);
            next();
        }
    })

    if(!checkPower(newFighter.power)){
        res.err = Error('Power is not valid');
    }
    console.log(newFighter.hasOwnProperty('health'), "health");
    if(newFighter.hasOwnProperty('health')){
        if(!checkHealth(newFighter.health)){
            res.err = Error('Health is not valid');
        }
    }

    if(!checkDefense(newFighter.defense)){
        res.err = Error('Defense is not valid');
    }   

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const newFighter = req.body;
    
    Object.keys(newFighter).forEach(key => {
        if (key === "id") {
            res.err = Error('id property is not allowed');
            next();
        } else if (Object.keys(fighter).indexOf(key) < 0) {
            res.err = Error(`${key} is not allowed attribute`);
            next();
        }
    })

    if(!checkPower(newFighter.power)){
        res.err = Error('Power is not valid');
    }
    console.log(newFighter.hasOwnProperty('health'), "health");
    if(newFighter.hasOwnProperty('health')){
        if(!checkHealth(newFighter.health)){
            res.err = Error('Health is not valid');
        }
    }

    if(!checkDefense(newFighter.defense)){
        res.err = Error('Defense is not valid');
    }
    next();
}

const checkPower = (power) => {
    return (power > 1 && power < 100) ? true: false;
}

const checkDefense = (defense) => {
    return (defense > 1 && defense < 10) ? true : false;
}


const checkHealth = (health) => {
    return (health > 80 && health< 120) ? true: false;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;