const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { fighter } = require('../models/fighter');
const e = require('express');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/',  (req, res, next)=>{
    try{
        
        res.data = FighterService.showAll();
    }
    catch(err){
        res.data = err;
    }
    finally{
        next()
    }  
}, responseMiddleware);

router.post('/', createFighterValid,  (req, res, next)=>{
    if(res.err){
        next();
    }else{
        try{
            const fighter = req.body;
            console.log(fighter);
            const data = FighterService.create(fighter);
            res.data = data;
        }
        catch(err){
            res.err = err;
        }
        finally{
            next();
        }
    }
}, responseMiddleware)

router.get('/:id', (req, res, next)=>{ 
    try{
        const id = req.params;
        console.log(id);
        const data = FighterService.search(id);
        res.data = data;
        console.log(data);
    }
    catch(err){
        res.err = err;
    }
    finally{
        next();
    }
}, responseMiddleware)


router.put('/:id', updateFighterValid, (req, res, next)=>{ 
    if(res.err){
        next();
    }
    try{
        const id = req.params.id;
        const user = req.body;
        const data = FighterService.update(id, user);
        res.data = data;
    }
    catch(err){
        res.err = err;
    }
    finally{
        next();
    }
}, responseMiddleware)

router.delete('/:id', (req, res, next)=>{ 
    try{
        const id = req.params.id;
        const data = FighterService.delete(id);
        res.data = data;
    }
    catch(err){
        res.err = err;
    }
    finally{
        next();
    }
}, responseMiddleware)

module.exports = router;