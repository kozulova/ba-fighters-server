const { responseMiddleware } = require('../middlewares/response.middleware');
const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(fighter){
        const item = FighterRepository.getOne(fighter);
        if(!item){
            if(!fighter.hasOwnProperty('health')){
                fighter.health = 100;
            }
            const newFighter = FighterRepository.create(fighter);
            return newFighter;
        }
        throw new Error('Sush fighter already exist');
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    showAll(){
        const all = FighterRepository.getAll();
        if(!all){
            return 'no fighter'
        }
        return all;
    }

    update(id, fighter){
        const item = FighterRepository.getOne({id});
        
        if(item){
            const updatedFighter = FighterRepository.update(id, fighter);
            return updatedFighter;
        }
        
        throw new Error ('Now such fighter to edit');
    }

    delete(id){
        const deletedFighter = FighterRepository.delete(id);
        if(!deletedFighter){
            return 'no delete'
        }
        return deletedFighter;
    }
}

module.exports = new FighterService();