const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    create(user){
        const item = UserRepository.getOne({email: user.email});
        if(!item){
            const newUser = UserRepository.create(user);
            return newUser;
        }
        throw new Error('Sush user already exist');
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('No such user');
        }
        return item;
    }

    showAll(){
        const all = UserRepository.getAll();
        if(!all){
            return 'no users'
        }
        return all;
    }

    update(id, user){
        const item = UserRepository.getOne({id});
        if(!item) {
            throw Error('No such user');
        }
        const updatedUser = UserRepository.update(id, user);
        return updatedUser;
    }

    delete(id){
        const deletedUser = UserRepository.delete(id);
        if(!deletedUser){
            return 'no delete'
        }
        return deletedUser;
    }
}

module.exports = new UserService();