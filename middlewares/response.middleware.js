const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if(res.data){
        res.status(200).send(`${JSON.stringify(res.data)}`);
    }else if(res.err){
        res.status(400).send({error: true, message: `${res.err.message}`});
    }
    else{
        next();
    }  
}

exports.responseMiddleware = responseMiddleware;