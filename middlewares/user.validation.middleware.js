const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const newUser = req.body;
    Object.keys(user).forEach(key => {
        if (key != 'id') {
            if (Object.keys(newUser).indexOf(key) < 0) {
                res.err = Error(`${key} is missing`);
                next()
            }
        }
    });

    Object.keys(newUser).forEach(key => {
        if (key === "id") {
            res.err = Error('id property is not allowed');
            next();
        } else if (Object.keys(user).indexOf(key) < 0) {
            res.err = Error(`${key} is not allowed attribute`)
            next();
        }
    })


    if (!validateEmail(newUser.email)) {
        res.err = Error("Not valid email");
    }
    if (!validatePhone(newUser.phoneNumber)) {
        res.err = Error("Not Valid phone");
    }
    if (!validatePassword(newUser.password)) {
        res.err = new Error("Too small password");
    }
    next();

}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const newUser = req.body;

    Object.keys(newUser).forEach(key => {
        if (key === "id") {
            res.err = Error('id property is not allowed');
            next();
        } else if (Object.keys(user).indexOf(key) < 0) {
            res.err = Error(`${key} is not allowed attribute`)
            next();
        }
    })

    if (!validateEmail(newUser.email)) {
        res.err = Error("Not valid email");
    }
    if (!validatePhone(newUser.phoneNumber)) {
        res.err = Error("Not Valid phone");
    }
    if (!validatePassword(newUser.password)) {
        res.err = Error("Too small password");
    }

    next();
}

const validateEmail = (email) => {
    const regexp = new RegExp(/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/);
    return regexp.test(email);
}

const validatePhone = (phone) => {
    const regexp = new RegExp(/^\+380[0-9]{7}/);
    return regexp.test(phone);
}

const validatePassword = (password) => {
    if (password.toString().length > 3) {
        return true
    }
    return false;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;